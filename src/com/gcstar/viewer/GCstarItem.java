/****************************************************
 *
 *  Copyright 2010 Christian Jodar
 *
 *  This file is part of GCstar Viewer.
 *
 *  GCstar Viewer is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  GCstar Viewer is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GCstar Viewer; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************/

package com.gcstar.viewer;

import java.util.ArrayList;
import java.util.HashMap;

public class GCstarItem
{
    public GCstarItem(GCstarModel model)
    {
        _values = new HashMap<String, String>();
        _lists = new HashMap<String, ArrayList<ArrayList<String>>>();
        _model = model;
    }

    public void addField(String key, String value)
    {
        _values.put(key, value);
    }

    public void addField(String key, ArrayList<ArrayList<String>> value)
    {
        // Dummy entry because we will iterate on _values
        _values.put(key, null);
        _lists.put(key, value);
    }

    public String getTitle()
    {
        return _values.get(_model.getTitleField());
    }

    public String getCover()
    {
        return _values.get(_model.getCoverField());
    }

    public HashMap<String, String> getValues()
    {
        return _values;
    }

    public ArrayList<ArrayList<String>> getList(String key)
    {
        return _lists.get(key);
    }

    public boolean hasField(String key)
    {
        return _values.containsKey(key);
    }

    private HashMap<String, String> _values;
    private HashMap<String, ArrayList<ArrayList<String>>> _lists;
    private GCstarModel _model;
};
