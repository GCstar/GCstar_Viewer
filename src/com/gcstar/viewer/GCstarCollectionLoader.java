/****************************************************
 *
 *  Copyright 2010 Christian Jodar
 *
 *  This file is part of GCstar Viewer.
 *
 *  GCstar Viewer is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  GCstar Viewer is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GCstar Viewer; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************/

package com.gcstar.viewer;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.xmlpull.v1.XmlPullParser;

import android.app.ProgressDialog;
import android.content.res.Resources;
import android.graphics.BitmapFactory.Options;
import android.os.AsyncTask;
import android.util.Xml;
import android.widget.ImageView;

public class GCstarCollectionLoader
{
    public GCstarCollectionLoader(GCstarViewer viewer)
    {
        _viewer = viewer;
        _isOrderedAscending = true;
        _df = android.text.format.DateFormat.getDateFormat(_viewer);
        _dfo = DateFormat.getDateInstance(DateFormat.SHORT, Locale.FRANCE);
        _isZip = false;
        _tmpImages = new HashMap<String, File>();
        _imgLoader = new GCstarImageLoader(_viewer, this);

        /*
        if (android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED))
        {
            _cacheDir = new File(android.os.Environment
                   .getExternalStorageDirectory() + "/Android/data/com.gcstar.viewer/cache/");
            //_cacheDir = viewer.getFilesDir();
        }
        else
        {
            _cacheDir = viewer.getCacheDir();
        }
        //_cacheDir = viewer.getCacheDir();
        if (!_cacheDir.exists() && ! _cacheDir.mkdirs())
        {
            _cacheDir = viewer.getCacheDir();
            _cacheDir.mkdirs();
        }
        */
        
        if (android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED))
        {
            //_cacheDir = new File(android.os.Environment
            //        .getExternalStorageDirectory(), ".gcstar");
                _cacheDir = new File(android.os.Environment.getExternalStorageDirectory(),
                                     "/Android/data/com.gcstar.viewer/cache");
        }
        else
        {
            _cacheDir = viewer.getCacheDir();
        }
        if (!_cacheDir.exists())
        {
            if (! _cacheDir.mkdirs())
            {
                System.out.println("ERROR CREATING " + _cacheDir
.getAbsolutePath());
            }
        }
    }
    
    public void finalize()
    {
        cleanup();
    }

    public synchronized void cleanup()
    {
        _imgLoader.clearCache();
        try
        {
            if (_isZip)
            {
                _zip.close();
                _isZip = false;
                cleanTempImages();
            }
        }
        catch (IOException ioe)
        {
            _gotErrors = true;
            return;
        }
    }

    public class LoaderTask extends
            AsyncTask<GCstarCollectionLoader, Integer, Integer>
    {
        protected Integer doInBackground(GCstarCollectionLoader... p)
        {
            _parent = p[0];
            _parent.loadFile(_parent._fileName);
            return 0;
        }

        protected void onPostExecute(Integer result)
        {
            _parent.updateViewer();
            _parent.stopDialog();
        }

        protected GCstarCollectionLoader _parent;
    };

    public void load(String fileName)
    {
        _gotErrors = false;
        _fileName = fileName;
        _dialog = ProgressDialog.show(_viewer, "", _viewer.getResources()
                .getString(R.string.loading), true);
        new LoaderTask().execute(this);
    }

    public class TitleSorter implements java.util.Comparator<GCstarItem>
    {
        public TitleSorter(boolean order)
        {
            _order = order;
        }

        public int compare(GCstarItem a, GCstarItem b)
        {
            if (_order)
            {
                return a.getTitle().compareToIgnoreCase(b.getTitle());
            }
            else
            {
                return b.getTitle().compareToIgnoreCase(a.getTitle());
            }
        }

        boolean _order;
    };

    protected void loadFile(String fileName)
    {
        // _model = new GCstarModel("games", this);
        cleanup();
        _items = new ArrayList<GCstarItem>();
        _collectionName = fileName;
        _isZip = false;
        InputStream in = null;
        try
        {
            File file = new File(fileName);
            if (!file.canRead())
            {
                _gotErrors = true;
                return;
            }
            if (fileName.endsWith(".gcz"))
            {
                try
                {
                    _zip = new ZipFile(file, ZipFile.OPEN_READ);
                    Enumeration<?> entries = _zip.entries();
                    while (entries.hasMoreElements())
                    {
                        ZipEntry entry = (ZipEntry) entries.nextElement();
                        if (entry.getName().endsWith(".gcs"))
                        {
                            in = _zip.getInputStream(entry);
                            break;
                        }
                    }
                    // _zip.close();
                    _isZip = true;
                }
                catch (IOException ioe)
                {
                    _gotErrors = true;
                    return;
                }
            }
            else
            {
                in = new FileInputStream(file);
                _currentDir = file.getParent();
            }
            if (in == null)
            {
                _gotErrors = true;
                return;
            }
            XmlPullParser parser = Xml.newPullParser();
            try
            {
                boolean isItem = false;
                boolean isCol = false;
                boolean isName = false;
                // auto-detect the encoding from the stream
                parser.setInput(in, null);
                parser.next();
                int eventType = parser.getEventType();
                GCstarItem currentItem = null;
                String currentTag = new String();
                ArrayList<ArrayList<String>> currentList = null;
                while (eventType != XmlPullParser.END_DOCUMENT)
                {
                    if (eventType == XmlPullParser.START_TAG)
                    {
                        String tag = parser.getName();
                        if (tag.equals("collection"))
                        {
                            String model = parser.getAttributeValue(null,
                                    "type");
                            _model = new GCstarModel(model.substring(2)
                                    .toLowerCase(), _viewer);
                        }
                        else if (tag.equals("name"))
                        {
                            isName = true;
                        }
                        else if (tag.equals("item"))
                        {
                            currentItem = new GCstarItem(_model);
                            isItem = true;
                            int count = parser.getAttributeCount();
                            for (int i = 0; i < count; i++)
                            {
                                String key = parser.getAttributeName(i);
                                String value = parser.getAttributeValue(i);
                                if (key.equals("borrower")
                                        && value.equals("none"))
                                {
                                    value = _model.getLabel("PanelNobody");
                                }
                                else if (_model.getFieldType(key) == GCstarModel.YesNo)
                                {
                                    int id = value.equals("1") ? R.string.yes
                                            : R.string.no;
                                    value = _viewer.getResources().getText(id)
                                            .toString();
                                }
                                else if (_model.getFieldType(key) == GCstarModel.Date)
                                {
                                    if ((value != null) && (value.length() > 0))
                                    {
                                        Date d = null;
                                        try
                                        {
                                            d = _dfo.parse(value);
                                            value = _df.format(d);
                                        }
                                        catch (Exception e)
                                        {

                                        }
                                    }
                                }
                                currentItem.addField(
                                        parser.getAttributeName(i), value);
                            }
                        }
                        else if (tag.equals("line"))
                        {
                            currentList.add(new ArrayList<String>());
                        }
                        else if (tag.equals("col"))
                        {
                            isCol = true;
                        }
                        else if (isItem)
                        {
                            currentTag = tag;
                            if (_model.isList(tag))
                            {
                                currentList = new ArrayList<ArrayList<String>>();
                            }
                        }
                    }
                    else if (eventType == XmlPullParser.END_TAG)
                    {
                        String tag = parser.getName();
                        if (tag.equals("item"))
                        {
                            _items.add(currentItem);
                            isItem = false;
                        }
                        else if (!(tag.equals("col") || tag.equals("line")))
                        {
                            if (_model.isList(currentTag))
                            {
                                if (_model.getFieldType(currentTag) == GCstarModel.MultiList)
                                {
                                    currentItem.addField(currentTag,
                                            currentList);
                                }
                                else
                                {
                                    String value = new String();
                                    int i = 0;
                                    for (ArrayList<String> line : currentList)
                                    {
                                        // ArrayList<String> line =
                                        // currentList.get(i);
                                        if (line.size() > 0)
                                        {
                                            value = value.concat(line.get(0));
                                            if (i < (currentList.size() - 1))
                                            {
                                                value = value.concat("\n");
                                            }
                                        }
                                        i++;
                                    }
                                    currentItem.addField(currentTag, value);
                                }
                            }
                            else if (_model.getFieldType(currentTag) == GCstarModel.LongText)
                            {
                                if (!currentItem.hasField(currentTag))
                                {
                                    currentItem.addField(currentTag, "");
                                }
                            }
                            currentTag = "";
                        }
                        else
                        {
                            if (tag.equals("col"))
                            {
                                isCol = false;
                            }
                        }
                    }
                    else if (eventType == XmlPullParser.TEXT)
                    {
                        String text = parser.getText();
                        if (isName)
                        {
                            if (text.length() > 0)
                            {
                                _collectionName = text;
                            }
                            isName = false;
                        }
                        else if (_model.isList(currentTag))
                        {
                            if (isCol)
                            {
                                currentList.get(currentList.size() - 1).add(
                                        text);
                            }
                        }
                        else if (isItem && (currentTag.length() != 0))
                        {
                            currentItem.addField(currentTag, text);
                        }
                    }
                    eventType = parser.next();
                }
            }
            catch (Exception e)
            {
                _gotErrors = true;
            }
        }
        catch (Exception e)
        {
            _gotErrors = true;
        }

        if (!_gotErrors)
        {
            Collections.sort(_items, new TitleSorter(_isOrderedAscending));
            _viewer.setModel(_model);
        }
    }

    public synchronized String convertImagePath(String path)
    {
        if (_isZip && path.length() > 4)
        {
            
            String ext = path.substring(path.length() - 4);
            try
            {
                File f = File.createTempFile(ImagesPrefix, ext, _cacheDir);
                f.deleteOnExit();
                BufferedOutputStream buf = new BufferedOutputStream(
                        new FileOutputStream(f),
                        2048);
                ZipEntry entry = _zip.getEntry(path);
                if (entry != null)
                {
                    BufferedInputStream is = new BufferedInputStream(
                            _zip.getInputStream(entry),
                            2048);
                    byte data[] = new byte[2048];
                    int count;
                    while ((count = is.read(data, 0, 2048)) != -1)
                    {
                        buf.write(data, 0, count);
                    }
                    buf.flush();
                    buf.close();
                    is.close();
                    _tmpImages.put(path, f);
                    return f.getAbsolutePath();
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        return path;
    }

    public synchronized void cleanTempImages()
    {
        Collection<File> c = _tmpImages.values();
        Iterator<File> itr = c.iterator();
        while (itr.hasNext())
        {
            File f = itr.next();
            f.delete();
        }
        _tmpImages.clear();

        // Force an extra cleanup
        File[] files = _cacheDir.listFiles(new FilenameFilter()
        {
            public boolean accept(File arg0, String arg1)
            {
                return arg1.startsWith(ImagesPrefix);
            }            
        });
        if (files != null)
        {
            for (File f : files)
            {
                f.delete();
            }
        }
    }

    public void reverseOrder()
    {
        _isOrderedAscending = !_isOrderedAscending;
        Collections.sort(_items, new TitleSorter(_isOrderedAscending));
        _viewer.setItems(_items, null);
    }

    public void stopDialog()
    {
        _dialog.dismiss();
    }

    public void updateViewer()
    {
        if (_gotErrors)
        {
            _viewer.chooseFile();
        }
        else
        {
            // set title
            _viewer.setTitle(_collectionName + " (" + _items.size() + ')');
            // set model
            _viewer.setModel(_model);
            // set list
            _viewer.setItems(_items, _fileName);
        }
    }

    public ArrayList<GCstarItem> getItems()
    {
        return _items;
    }

    public String getFileName()
    {
        return _fileName;
    }

    public boolean isZip()
    {
        return _isZip;
    }

    public void loadImage(ImageView imageView, String path, boolean isCover,
            int height, Options options, boolean cache)
    {
        imageView.setTag(path);
        _imgLoader.displayImage(path, _viewer, imageView, height, isCover,
                cache, options);
    }

    public String getCurrentDir()
    {
        return _currentDir;
    }

    public String getCollectionName()
    {
        return _collectionName;
    }

    public Resources getResources()
    {
        return _viewer.getResources();
    }

    private String _fileName;
    private GCstarViewer _viewer;
    private ArrayList<GCstarItem> _items;
    private String _currentDir;
    private GCstarModel _model;
    private String _collectionName;
    private ProgressDialog _dialog;
    private boolean _isOrderedAscending;
    private DateFormat _df;
    private DateFormat _dfo;
    private boolean _gotErrors;
    private boolean _isZip;
    private ZipFile _zip;
    private HashMap<String, File> _tmpImages;
    private GCstarImageLoader _imgLoader;
    private File _cacheDir;
    private final static String ImagesPrefix = "gcstar_";
};
